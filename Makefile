###########################################################
# Developer's Guide
###########################################################
#
# All tasks should be explicitly marked as .PHONY at the
# top of the section.
#
# We distinguish two types of tasks: private and public.
#
# "Public" tasks should be created with the description
# using ## comment format:
#
#   public-task: task-dependency ## Task description
#
# Private tasks should start with "_". There should be no
# description E.g.:
#
#   _private-task: task-dependency
#

###########################################################
# Utils
###########################################################

#--------------------------------------
# Colors
#--------------------------------------

_interactive:=$(shell [ -t 0 ] && echo 1)

ifdef _interactive
# Error messages (red)
_EC := \033[0;31m
# Run messages (green)
_RC := \033[0;32m
# Build messages (yellow)
_BC	:= \033[0;33m
# Setup and install messages (blue)
_SC := \033[0;34m
# No coloring
_NC := \033[0m
endif

#--------------------------------------
# Printing
#--------------------------------------
# Output prefix
_OPX := "[MAKE] - "

###########################################################
# Help
###########################################################
.PHONY: help

help: ## Shows help
	@printf "\033[33m%s:\033[0m\n" 'Use: make <command> where <command> one of the following'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

###########################################################
# Project directories & paths
###########################################################

root_dir := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

###########################################################
# Config
###########################################################

project_name := dind-rust
image_name := registry.gitlab.com/mavka/etc/$(project_name)

#--------------------------------------
# Image
#--------------------------------------

smallest_semver_version := 0.0.0-alpha
version := $(shell git describe --tags --abbrev=0 2>/dev/null || echo $(smallest_semver_version))
image_tag := $(version)

#--------------------------------------
# Dependencies
#--------------------------------------

# Include VERSIONS file
ifneq (,$(wildcard ./VERSIONS))
    include VERSIONS
    export
endif

docker_version = $(DOCKER_VERSION)
ifeq ($(DOCKER_VERSION),)
docker_version := latest
endif

apple_codesign_version := $(APPLE_CODESIGN_VERSION)
ifeq ($(APPLE_CODESIGN_VERSION),)
unexport apple_codesign_version
endif

#--------------------------------------
# Docker
#--------------------------------------

platform := linux/arm64,linux/amd64

###########################################################
# Info
###########################################################
.PHONY: info

info: ## Publish project info
	@echo "$(_RC)$(_OPX)Docker version:            $(_NC)   $(_SC)$(docker_version)$(_NC)"
	@echo "$(_RC)$(_OPX)Apple Code Signing version:$(_NC)   $(_SC)$(apple_codesign_version)$(_NC)"
	@echo "$(_RC)$(_OPX)Version:                   $(_NC)   $(_SC)$(version)$(_NC)"
	@echo "$(_RC)$(_OPX)Versioned image tag:       $(_NC)   $(_SC)$(image_name):$(image_tag)$(_NC)"

###########################################################
# Build
###########################################################
.PHONY: build

build: info ## Build image
	@echo "$(_BC)$(_OPX)Building image...$(_NC)"
	@docker buildx build \
		--build-arg DOCKER_VERSION=$(docker_version) \
		--platform $(platform) \
		.

###########################################################
# Publish
###########################################################
.PHONY: push

push: info ## Publish image to container registry (build if necessary)
	@echo "$(_BC)$(_OPX)Building and pushing image to container registry...$(_NC)"
	@docker buildx build \
		--build-arg DOCKER_VERSION=$(docker_version) \
		--build-arg APPLE_CODESIGN_VERSION=$(apple_codesign_version) \
		--platform $(platform) \
		--tag $(image_name):$(image_tag) \
		--tag $(image_name):latest \
		--push \
		.
