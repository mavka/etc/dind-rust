Docker-in-Docker with Rust
==========================

Package Rust and install tools into image with Docker support.

Content
-------

- [`rustup`](https://rustup.rs/#).
- Latest stable Rust environment with default profile.
- [`Apple Code Signing`](https://gregoryszorc.com/docs/apple-codesign/stable/).
- A set of useful CI [scripts](./bin).

Usage
-----

> **Note**
> 
> Since we perform multi-platform builds, you have to create (if necessary) and activate Docker builder with
> multiplatform support. For example:
> 
> ```shell
> docker buildx create --driver docker-container --platform linux/arm64,linux/amd64 --name mutliarch-builder --use
> ```
> 
> Later you could activate this builder by:
> 
> ```shell
> docker buildx use mutliarch-builder
> ```

Build images (creates internal builder cache):

```shell
make build
```

Publish images to container registry:

```shell
make push
```

Acknowledgements
----------------

I've used a free [Rust icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/rust) and
[Docker icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/docker) to create project avatar, and I am
thankful for that. They require to post about this fact in social network, hope that GitLab counts ツ.

License
-------

> Here we simply comply with the suggested dual licensing according to
> [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/about.html) (C-PERMISSIVE).

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

Contribution
------------

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
