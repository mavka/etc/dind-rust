ARG DOCKER_VERSION="latest"

FROM docker.io/docker:${DOCKER_VERSION}

# Install rustup
RUN apk update && apk add --no-cache \
    build-base \
    rustup

# Install Rust
RUN rustup-init -y
ENV PATH="/root/.cargo/bin:${PATH}"

# Install Apple Code Signing
ARG APPLE_CODESIGN_VERSION="0.26.0"
RUN cargo install apple-codesign@${APPLE_CODESIGN_VERSION}

# Extra system dependencies
RUN apk update && apk add --no-cache \
    curl \
    git \
    jq \
    bash

# Install CLI tools
RUN mkdir -p /tmp/dind-rust/bin
COPY bin/* /tmp/dind-rust/bin
RUN chmod +x /tmp/dind-rust/bin/* && cp /tmp/dind-rust/bin/* /usr/local/bin/ && rm -rf /tmp/dind-rust/bin

# Setup working directory
RUN mkdir -p /work
WORKDIR /work
